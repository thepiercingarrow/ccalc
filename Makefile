CC = gcc
CFLAGS ?= -Wall -O2 -flto
BUILD = ccalc
C_FILES = $(wildcard src/*.c)
H_FILES = $(wildcard src/*.h)
OBJECTS = $(C_FILES:src/%.c=obj/%.o)

.PHONY = all install uninstall clean

all: ccalc

ccalc: $(OBJECTS)
	$(CC) $(CFLAGS) $(OBJECTS) -o $@

obj:
	mkdir -p obj/

obj/main.o: src/main.c obj src/stack.h src/queue.h
	$(CC) $(CFLAGS) -c $< -o $@

obj/rpn.o: src/rpn.c obj src/stack.h src/queue.h
	$(CC) $(CFLAGS) -c $< -o $@

install: ccalc
	cp $(BUILD) /usr/local/bin/

uninstall: ccalc
	rm /usr/local/bin/$(BUILD)

clean:
	rm -rf obj/
	-rm $(BUILD)
