[![Build Status](https://travis-ci.org/thepiercingarrow2/ccalc.svg?branch=master)](https://travis-ci.org/thepiercingarrow2/ccalc)

# ccalc

A powerful and versatile command-line calculator.

## Installation

Compile with `make`.  
Install with `sudo make install`.

## Usage

To launch the REPL, just run `ccalc`.

## Contributing

1. Fork it!
2. Create your feature branch: `git checkout -b my-new-feature`
3. Commit your changes: `git commit -am 'Add some feature'`
4. Push to the branch: `git push origin my-new-feature`
5. Submit a pull request :D

## License (MIT)

```
WWWWWW||WWWWWW
 W W W||W W W
      ||
    ( OO )__________
     /  |           \
    /o o|    MIT     \
    \___/||_||__||_|| *
         || ||  || ||
        _||_|| _||_||
       (__|__|(__|__|
```

Copyright (c) 2016 Maxwell Zhao <thepiercingarrow@gmail.com>.  
This project is licensed under the MIT/X11 lisence. Read [LICENSE.txt](LICENSE.txt) for more details.
