#include <stdio.h>
#include <stdlib.h>

#include "stack.h"
#include "queue.h"

#include "rpn.h"
#include "shuntyard.h"

int main(int argc, char *argv[]) {
    if (argc == 1)
        repl();
    else if (*argv[1] == '/') {
        // TODO
    }
    else {
        printf("%Lg\n", rpn(shuntyard(argv[1], argc), argc));
    }
}
