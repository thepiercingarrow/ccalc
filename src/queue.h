#ifndef QUEUE_H
#define QUEUE_H

#define queue(q) q##_a, q##_b; q##_a = q##_b

#define enqueue(q,a) (*(q##_b)++ = (a))
#define dequeue (*(q##_a)++)

#endif
