#ifndef STACK_H
#define STACK_H

#define stack *

#define push(s,a) (*(s)++=(a))
#define pop(s) (*--(s))

#define stack_index(s,i) (*((s)-i))

#endif
